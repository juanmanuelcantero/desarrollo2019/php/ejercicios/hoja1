<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>21</title>
    </head>
    <body>
        <?php
        $numeros = [
        'a' => 8,
        'b' => 6
        ];
        
        $a = $numeros['a'];
        $b = $numeros['b'];
        
        if ($a > $b) {
            print "a es mayor que b<br>";
            $b = $a;
        }
        
        if ($a > $b):
            print "A es mayor que B<br>";
        endif;
        
        if ($a > $b) {
            print "a es mayor que b<br>";
        } elseif ($a == $b) {
            print "a es igual que b<br>";
        } else {
            print "b es mayor que a<br>";
        }

        if ($a > $b):
            print "A es mayor que B<br>";
            print "...";
        elseif ($a == $b):
            print "A es igual que B<br>";
            print "!!!";
        else:
            print "B es mayor que A<br>";
        endif;

        ?>
    </body>
</html>

